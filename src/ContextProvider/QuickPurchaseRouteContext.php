<?php

namespace Drupal\commerce_quick_purchase\ContextProvider;

use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Sets the current commerce_product or commerce_store as the route context.
 */
class QuickPurchaseRouteContext implements ContextProviderInterface {

  use StringTranslationTrait;

  /**
   * The route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The current store object.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;

  /**
   * Constructs a new QuickPurchaseRouteContext.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   */
  public function __construct(RouteMatchInterface $route_match, CurrentStoreInterface $current_store) {
    $this->routeMatch = $route_match;
    $this->currentStore = $current_store;
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    $result = [];

    $route = $this->routeMatch;

    $cacheability = new CacheableMetadata();
    $cacheability->setCacheContexts(['route']);

    if ($route_object = $route->getRouteObject()) {
      $route_contexts = $route_object->getOption('parameters');

      $value = NULL;
      if (isset($route_contexts['commerce_product'])) {
        if ($commerce_product = $route->getParameter('commerce_product')) {
          $value = $commerce_product;
        }
      }
      elseif ($route->getRouteName() === 'entity.commerce_product.add_form') {
        $commerce_product_type = $route->getParameter('commerce_product_type');
        $value = Product::create(['type' => $commerce_product_type->id()]);
      }

      $context = new Context(EntityContextDefinition::fromEntityTypeId('commerce_product'), $value);
      $context->addCacheableDependency($cacheability);
      $result['commerce_product'] = $context;
    }

    $context = new Context(EntityContextDefinition::fromEntityTypeId('commerce_store'),  $this->currentStore->getStore());
    $context->addCacheableDependency($cacheability);
    $result['commerce_store'] = $context;

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts() {
    $product_context = EntityContext::fromEntityTypeId('commerce_product', $this->t('Product from URL if available'));
    $store_context = EntityContext::fromEntityTypeId('commerce_store', $this->t('Current store from store resolution'));
    return ['commerce_product' => $product_context, 'commerce_store' => $store_context];
  }

}
